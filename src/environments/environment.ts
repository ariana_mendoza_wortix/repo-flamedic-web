export const environment = {
    production: false,

    region: 'us-east-1',

    identityPoolId: 'us-east-1:79a6cbd6-6b7d-467f-ad22-a1e8bb9582f3',
    userPoolId: 'us-east-1_DfBuMKjzt',
    clientId: '6ucjbp5ptn22d8gqsbaef05cc6',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-1',

    ddbTableName: 'LoginTrail'
};

