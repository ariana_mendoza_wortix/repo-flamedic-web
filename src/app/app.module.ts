import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {UserRegistrationService, UserLoginService, UserParametersService, CognitoUtil} from "./service/cognito.service";
import {CompanyService} from "./service/api/company.service";
import {CitaService} from "./service/api/cita.service";
import {PatientService} from "./service/api/patient.service";
import {PatientComponent} from "./secure/patient/patient.component";
import {DoctorService} from "./service/api/doctor.service";
import {DoctorComponent} from "./secure/useractivity/doctor.component";
import {routing} from "./app.routes";
import {HomeComponent, AboutComponent, HomeLandingComponent} from "./public/home.component";
import {AwsUtil} from "./service/aws.service";
import {UseractivityComponent} from "./secure/useractivity/useractivity.component";
import {MyProfileComponentList,MyProfileComponent} from "./secure/profile/myprofile.component";
import {SecureHomeComponent} from "./secure/landing/securehome.component";
import {JwtComponent} from "./secure/jwttokens/jwt.component";
import {DynamoDBService} from "./service/ddb.service";
import {LoginComponent} from "./public/auth/login/login.component";
import {RegisterComponent} from "./public/auth/register/registration.component";
import {ForgotPasswordStep1Component, ForgotPassword2Component} from "./public/auth/forgot/forgotPassword.component";
import {LogoutComponent, RegistrationConfirmationComponent} from "./public/auth/confirm/confirmRegistration.component";
import {RegistrationCompanyComponent} from "./public/auth/registerCompany/companyRegistration.component";
import {ResendCodeComponent} from "./public/auth/resend/resendCode.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CalendarModule } from "angular-calendar";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';





@NgModule({
    declarations: [
        LoginComponent,
        LogoutComponent,
        RegistrationConfirmationComponent,
        RegistrationCompanyComponent,
        ResendCodeComponent,
        ForgotPasswordStep1Component,
        ForgotPassword2Component,
        RegisterComponent,
        AboutComponent,
        HomeLandingComponent,
        HomeComponent,
        UseractivityComponent,
        MyProfileComponent,
        MyProfileComponentList,
        SecureHomeComponent,
        JwtComponent,
        PatientComponent,
        DoctorComponent,
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        BrowserAnimationsModule,
        NgbModule,
        CalendarModule.forRoot()
        
        
    ],
    providers: [
        CognitoUtil,
        AwsUtil,
        DynamoDBService,
        CompanyService,
        PatientService,
        DoctorService,
        CitaService,
        UserRegistrationService,
        UserLoginService,
        UserParametersService],
        
    bootstrap: [AppComponent]
})
export class AppModule {
}
