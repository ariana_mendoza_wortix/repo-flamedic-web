import {Injectable , Inject} from '@angular/core';
import {Http,HttpModule, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

const BASE_URL = 'https://9wdpgncxrb.execute-api.us-east-1.amazonaws.com/dev/company_patients/7cf50c30-4d0c-11e7-a7f3-578be3554a08';
const BASE_URL_CREATE = 'https://9wdpgncxrb.execute-api.us-east-1.amazonaws.com/dev/patients';
const HEADER = { headers: new Headers({ 'Content-Type': 'application/json' }) };

export class PatientClass{
  createdByName: string;
  patientId:string;
  firstName: string;
  lastName:string;
  companyId: string;
  company: string;
  docId: string;
  lowPatientName: string;
  updatedAt: string;
  createdAt: string;
  patientName: string;
  email: string;
  phone: number;
  gender:string;
  birthday: Date;
  createdById:string;
  ownerId: string;
};

export interface Patient{
  createdByName: string,
  patientId:string,
  firstName: string,
  lastName:string,
  companyId: string,
  company: string,
  docId: string,
  lowPatientName: string,
  updatedAt: string,
  createdAt: string,
  patientName: string,
  email: string,
  phone: number,
  gender:string,
  birthday: Date,
  createdById:string,
  ownerId: string,
};


@Injectable()

export class PatientService { 

    patients:Array<any>;
    patientsList:Array<Patient>;
    
    constructor(private http:Http) 
        {
            
    }
    getPatients():Observable<Patient[]>{        
        return this.http.get(BASE_URL,{ headers: this.getHeaders()})
                        .map(this.extractData)
                        .do(data => console.log("get Patients from json: " + JSON.stringify(data)))
                        .catch(this.handleError);
    }
    createPatient(patient: PatientClass):Observable<Response> {
    /*this.patientsList = <Patient[]>({
        patientsList.firstName: patient.firstName,
     });*/
    
    return this.http.post(BASE_URL_CREATE, JSON.stringify(patient), HEADER)
      .map(res => res.json())
      .catch(this.handleError);
  }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
      
     
    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || "500 internal server error");
    }
    private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
}