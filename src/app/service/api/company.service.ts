import {Injectable} from '@angular/core';
import {Http,HttpModule, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

const BASE_URL = 'src/assets/data/company.json';
const HEADER = { headers: new Headers({ 'Content-Type': 'application/json' }) };

export interface Company {
  id: number;
  name: string;
  description;
};

@Injectable()

export class CompanyService { 

    companys:Array<any>;
    
    constructor(private http:Http) 
        {
            
    }
    getCompanys():Observable<Company[]>{        
        return this.http.get(BASE_URL,{ headers: this.getHeaders()})
                        .map(this.extractData)
                        .do(data => console.log("get Companys from json: " + JSON.stringify(data)))
                        .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
      
     
    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || "500 internal server error");
    }
    private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
}