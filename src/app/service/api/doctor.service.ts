import {Injectable} from '@angular/core';
import {Http,HttpModule, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

const BASE_URL = 'https://9wdpgncxrb.execute-api.us-east-1.amazonaws.com/dev/doctors/7cf50c30-4d0c-11e7-a7f3-578be3554a08';
const HEADER = { headers: new Headers({ 'Content-Type': 'application/json' }) };



export interface Doctor{
  companyId: string,
  lastName: string,
  userId: string,
  updatedAt: number,
  specialtyId:string,
  createdAt: number,
  availableCalendar:string;
  firstName: string,
  doctorId: string;
  
};


@Injectable()

export class DoctorService { 

    doctor:Array<any>;
    
    constructor(private http:Http) 
        {
            
    }
    getDoctores():Observable<Doctor[]>{        
        return this.http.get(BASE_URL,{ headers: this.getHeaders()})
                        .map(this.extractData)
                        .do(data => console.log("get Doctores from json: " + JSON.stringify(data)))
                        .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
      
     
    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || "500 internal server error");
    }
    private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
}