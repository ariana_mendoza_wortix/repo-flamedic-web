import {Component, OnInit} from "@angular/core";
import {CompanyService, Company} from "../../service/api/company.service";
import {CitaService, Cita} from "../../service/api/cita.service";
import {LoggedInCallback, UserLoginService, UserParametersService, Callback} from "../../service/cognito.service";
import {Router} from "@angular/router";
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';  
import { Pipe } from "@angular/core";


declare var AWS: any;
declare var jQuery:any;
declare var $:any;
const BASE_URL = 'src/assets/data/company.json';

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './myprofile.html'
})
export class MyProfileComponent implements OnInit, LoggedInCallback {

    public parameters: Array<Parameters> = [];
    public cognitoId: String;
    public companyList:Array<Company>=[];
    public errorMessage: string;
    public citasList:Array<Cita>=[];
    
    constructor(public router: Router, public userService: UserLoginService, 
    public userParams: UserParametersService, public _companyService: CompanyService,public _citaService: CitaService, public http:Http) {
        this.userService.isAuthenticated(this);
        console.log(_companyService.getCompanys());
        
    }
     ngOnInit() {
       
       this._companyService.getCompanys()
                .subscribe(res => this.companyList = res);
       console.log(this.companyList); 
       this._citaService.getCitas()
                .subscribe(res => this.citasList = res);
       console.log(this.citasList);  
       this.loadScrooll();  
       this.inputMaskHora();
    }
    inputMaskHora(){
        
   jQuery(function($){
      $.mask.definitions['H']='[012]';
      $.mask.definitions['N']='[012345]';
      $.mask.definitions['n']='[0123456789]';
      $("#time").mask("Hn:Nn:Nn");
       });
    }
    
    loadScrooll(){
        $(document).ready(function(){

        $("#lista-citas").mCustomScrollbar({
            theme:"3d-dark"
       });
    
     });
  }
    

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.userParams.getParameters(new GetParametersCallback(this));
            
        }
    }
    
}
    


export class Parameters {
    name: string;
    value: string;
}

export class GetParametersCallback implements Callback {

    constructor(public me: MyProfileComponent) {

    }

    callback() {

    }

    callbackWithParam(result: any) {

        for (let i = 0; i < result.length; i++) {
            let parameter = new Parameters();
            parameter.name = result[i].getName();
            parameter.value = result[i].getValue();
            this.me.parameters.push(parameter);
        }
        let param = new Parameters()
        param.name = "cognito ID";
        param.value = AWS.config.credentials.params.IdentityId;
        this.me.parameters.push(param)
    }
}





@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './calendario.html'
})

export class MyProfileComponentList implements OnInit, LoggedInCallback {

    public parameters: Array<Parameters> = [];
    public cognitoId: String;
    public companyList:Array<Company> = [];
    public errorMessage: string;
    public indexes:Array<Company>=[];
    public citasList:Array<Cita>=[];
    

 constructor(public router: Router, public userService: UserLoginService, public userParams: UserParametersService,public _companyService: CompanyService,public _citaService: CitaService,public http:Http) {
        this.userService.isAuthenticated(this);
        console.log("In MyProfileComponent");
    }
     ngOnInit() {
     
     this.loadFullCalendar();
     this.inputMaskHora();
       
    }
    loadScrooll(){
        $(document).ready(function(){

        $("#lista-citas").mCustomScrollbar({
            theme:"3d-dark"
       });
    
     });
  }
    inputMaskHora(){
        
    jQuery(function($){
      $.mask.definitions['H']='[012]';
      $.mask.definitions['N']='[012345]';
      $.mask.definitions['n']='[0123456789]';
      $("#time").mask("Hn:Nn:Nn");
       });
    }
    
    loadFullCalendar(){
        
        /*var initialLocaleCode = 'es';*/
        $(document).ready(function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        /*  className colors
        
        className: default(transparent), important(red), chill(pink), success(green), info(blue)
        
        */      
        
          
        /* initialize the external events
        -----------------------------------------------------------------*/
    
        $('#external-events div.external-event').each(function() {
        
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()), // use the element's text as the event title
                description:$.trim($(this).text())
            };
            
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
            
        });
    
    
        /* initialize the calendar
        -----------------------------------------------------------------*/
        
        var calendar =  $('#calendar').fullCalendar({
            
            header: {
                left: 'title',
                center: 'agendaDay,agendaWeek,month',
                right: 'prev,next today'
            },
            views: {
                listDay: { buttonText: 'list day' },
                listWeek: { buttonText: 'list week' }
            },
            /*locale: initialLocaleCode,*/
            editable: true,
            firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: true,
            defaultView: 'month',
            
            axisFormat: 'h:mm',
            columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
            allDaySlot: false,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Título:');
                var dateStart= 'Fecha de evento:' ;
                if (title) {
                    calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            dateStart:dateStart,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                }
                calendar.fullCalendar('unselect');
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped
            
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
                
            },
            
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1)
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false,
                    className: 'info'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false,
                    className: 'info'
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    className: 'info'
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    className: 'important'
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false,
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    className: 'success'
                }
            ],          
        });
        
        
    });
    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.userParams.getParameters(new GetParametersCallback(this));
        }
    }
    
    
    
    
    
    
    
}
