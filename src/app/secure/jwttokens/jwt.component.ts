import {Component, OnInit} from "@angular/core";
import {UserRegistrationService, CognitoCallback,LoggedInCallback, UserLoginService, CognitoUtil, Callback} from "../../service/cognito.service";
import {Router} from "@angular/router";
import {PatientService, Patient, PatientClass} from "../../service/api/patient.service";


declare var jQuery:any;
declare var $:any;
declare var intelInput:string;
declare var Areemplazar:string;
declare var reemplazado:string;


export class Stuff {
    public accessToken: string;
    public idToken: string;
}

export class RegistrationUser {
    name: string;
    email: string;
    password: string;
    surnames: string;
    identity: string;   
    birthdate: Date;
    civil: string;
    job: string;
    gender: string;
    insurance: string;
    skin: string;
    weight:number;
    height:number;
    blood: string; 
    phone:number;
    company: string;
    customType:string;
    
    
}

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './registerPatient.html'
})
export class JwtComponent implements OnInit, LoggedInCallback {

    public stuff: Stuff = new Stuff();
    registrationUser: RegistrationUser;
    patientclass: PatientClass;
    errorMessage: string;
    intelInput: string;
    Areemplazar:string;
    reemplazado:string;

    constructor(public router: Router, public userService: UserLoginService,
     public cognitoUtil: CognitoUtil, public userRegistration: UserRegistrationService , public _patientService: PatientService,) {
        this.userService.isAuthenticated(this);
        console.log("in JwtComponent");

    }
     ngOnInit() {
        this.registrationUser = new RegistrationUser();
        this.patientclass     = new PatientClass();
        this.loadIntlTelInput();
        this.loadDatepicker();
        this.visibleFinish();
        
    }
    
    onRegister() {
        this.errorMessage = null;
        this.intelInput= $("#phone").intlTelInput("getNumber");
        console.log(this.intelInput);
        
        this.patientclass.firstName = this.registrationUser.name;
        this.patientclass.lastName  = this.registrationUser.surnames;
        this.patientclass.birthday  = this.registrationUser.birthdate;
        this.patientclass.phone     = this.registrationUser.phone;
        this.patientclass.docId     = this.registrationUser.identity;
        this.patientclass.company   = this.registrationUser.company;
        this.patientclass.email     = this.registrationUser.email;
        this.patientclass.ownerId   = this.registrationUser.email;
        if(this.registrationUser.email!=""){
            this.Areemplazar=this.registrationUser.email;
            this.reemplazado=Areemplazar.replace("@", "");
         }
        
        this.userRegistration.register(this.registrationUser,this.intelInput, this);
        this._patientService.createPatient(this.patientclass);
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            this.errorMessage = message;
            console.log("result: " + this.errorMessage);
        } else { //success
            //move to the next step
            console.log("redirecting");
             //this.router.navigate(['/home/confirmRegistration', result.user.username]);
        }
    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
            this.cognitoUtil.getIdToken(new IdTokenCallback(this));
        }
    }
    loadIntlTelInput(){
         $(document).ready(function () {
               $("#phone").intlTelInput();
            });
        
    }
    visibleFinish(){
         $(document).ready(function () {
               $("#finish").css("display","none");
            });
        
    }
    
    
    
    loadDatepicker(){
        $(document).ready(function () {
                
                $('#birthdate').datepicker({
                    format: "dd/mm/yyyy"
                }); 
                });
    }
}

export class AccessTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result) {
        this.jwt.stuff.accessToken = result;
    }
}

export class IdTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result) {
        this.jwt.stuff.idToken = result;
    }
}
