import {Component , OnInit} from "@angular/core";
import {LoggedInCallback, UserLoginService} from "../../service/cognito.service";
import {Router} from "@angular/router";
import {DynamoDBService} from "../../service/ddb.service";
import {DoctorService, Doctor} from "../../service/api/doctor.service";
import {Http} from '@angular/http';

declare var jQuery:any;
declare var $:any;
declare var intelInput:string;


export class Stuff {
    public type: string;
    public date: string;
}

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './doctor.html'
})
export class DoctorComponent  implements OnInit, LoggedInCallback {

    public logdata: Array<Stuff> = [];
    public doctortList:Array<Doctor>=[];

    constructor(public router: Router, public ddb: DynamoDBService, 
    public userService: UserLoginService, public _doctorService: DoctorService, public http:Http) {
        this.userService.isAuthenticated(this);
        console.log("in UseractivityComponent");
    }
    ngOnInit() {
        this.loadIntlTelInput();
        this.visibleFinish();
        this._doctorService.getDoctores()
                .subscribe(res => this.doctortList = res);
       console.log(this.doctortList); 
        
    }
    visibleFinish(){
         $(document).ready(function () {
               $("#finish").css("display","none");
            });
        
    }
    loadIntlTelInput(){
         $(document).ready(function () {
               $("#phone").intlTelInput();
            });
        
    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            console.log("scanning DDB");
            this.ddb.getLogEntries(this.logdata);
        }
    }

}
