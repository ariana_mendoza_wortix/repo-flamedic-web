import {Component , OnInit} from "@angular/core";
import {UserRegistrationService, CognitoCallback,LoggedInCallback, UserLoginService} from "../../service/cognito.service";
import {Router} from "@angular/router";
import {DynamoDBService} from "../../service/ddb.service";

declare var jQuery:any;
declare var $:any;
declare var intelInput:string;


export class Stuff {
    public type: string;
    public date: string;
}

export class RegistrationUser {
    name: string;
    email: string;
    password: string;
    surnames: string;
    identity: string;   
    birthdate: Date;
    civil: string;
    job: string;
    gender: string;
    insurance: string;
    skin: string;
    weight:number;
    height:number;
    blood: string; 
    phone:number;
    company: string;
    customType:string;
    
    
}

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './registerDoctor.html'
})
export class UseractivityComponent  implements OnInit, LoggedInCallback {

    public logdata: Array<Stuff> = [];
    registrationUser: RegistrationUser;
    errorMessage: string;
    intelInput: string;

    constructor(public router: Router, public ddb: DynamoDBService,
     public userService: UserLoginService,public userRegistration: UserRegistrationService) {
        this.userService.isAuthenticated(this);
        console.log("in UseractivityComponent");
    }
    ngOnInit() {
        this.registrationUser = new RegistrationUser();
        this.loadIntlTelInput();
        this.visibleFinish();
        
    }
    onRegister() {
        this.errorMessage = null;
         this.intelInput= $("#phone").intlTelInput("getNumber");
        
        console.log(this.intelInput);
        this.userRegistration.register(this.registrationUser,this.intelInput, this);
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            this.errorMessage = message;
            console.log("result: " + this.errorMessage);
        } else { //success
            //move to the next step
            console.log("redirecting");
             //this.router.navigate(['/home/confirmRegistration', result.user.username]);
        }
    }
    visibleFinish(){
         $(document).ready(function () {
               $("#finish").css("display","none");
            });
        
    }
    loadIntlTelInput(){
         $(document).ready(function () {
               $("#phone").intlTelInput();
            });
        
    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            console.log("scanning DDB");
            this.ddb.getLogEntries(this.logdata);
        }
    }

}
