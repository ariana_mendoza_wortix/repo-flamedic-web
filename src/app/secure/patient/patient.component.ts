import {Component, OnInit} from "@angular/core";
import {CompanyService, Company} from "../../service/api/company.service";
import {CitaService, Cita} from "../../service/api/cita.service";
import {PatientService, Patient} from "../../service/api/patient.service";
import {LoggedInCallback, UserLoginService, UserParametersService, Callback} from "../../service/cognito.service";
import {Router} from "@angular/router";
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';  
import { Pipe } from "@angular/core";


declare var AWS: any;
declare var jQuery:any;
declare var $:any;

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './patient.html'
})
export class PatientComponent implements OnInit, LoggedInCallback {

    public parameters: Array<Parameters> = [];
    public cognitoId: String;
    public patientList:Array<Patient>=[];
    
    
    constructor(public router: Router, public userService: UserLoginService, 
    public userParams: UserParametersService, public _patientService: PatientService, public http:Http) {
        this.userService.isAuthenticated(this); 
    }
     ngOnInit() {
       
       this._patientService.getPatients()
                .subscribe(res => this.patientList = res);
       console.log(this.patientList); 
      
       this.loadScrooll();  
    }
    
    loadScrooll(){
        $(document).ready(function(){

        $("#patientList").mCustomScrollbar({
            theme:"3d-dark"
       });
    
     });
  }
    

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.userParams.getParameters(new GetParametersCallback(this));
            
        }
    }
    
}
    


export class Parameters {
    name: string;
    value: string;
}

export class GetParametersCallback implements Callback {

    constructor(public me: PatientComponent) {

    }

    callback() {

    }

    callbackWithParam(result: any) {

        for (let i = 0; i < result.length; i++) {
            let parameter = new Parameters();
            parameter.name = result[i].getName();
            parameter.value = result[i].getValue();
            this.me.parameters.push(parameter);
        }
        let param = new Parameters()
        param.name = "cognito ID";
        param.value = AWS.config.credentials.params.IdentityId;
        this.me.parameters.push(param)
    }
}





